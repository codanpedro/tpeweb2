<?php
require_once './libs/smarty-3.1.39/libs/Smarty.class.php';

class LoginView{

    private $smarty;

    function __construct() {
        $this->smarty = new Smarty();
    }

    function showLogin(){
        $this->smarty->assign('titulo', 'Log In');
        $this->smarty->display('templates/Login/login.tpl');       
    }

    function showHomeLocation(){
        header("Location: ".BASE_URL."showHome");
    }

    function renderCamposIncompatibles(){
        $this->smarty->assign('error', 'El usuario no coincide con la contraseña');
        $this->smarty->display('templates/error.tpl');
    }

    function renderCamposIncompletos(){
        $this->smarty->assign('error', 'Faltaron completar campos');
        $this->smarty->display('templates/error.tpl');
    }


}