<?php
require_once './libs/smarty-3.1.39/libs/Smarty.class.php';

class UsuariosView{

    private $smarty;

    function __construct() {
        $this->smarty = new Smarty();
    }

    function showHomeLocation(){
        header("Location: ".BASE_URL."showHome");
    }

    function showUsuariosLocation(){
        header("Location: ".BASE_URL."showUsuarios");
    }

    function renderUsuarios($usuarios, $admin){
        $this->smarty->assign('usuarios', $usuarios);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('templates/Usuarios/usuarios.tpl');
    }

    function deleteUsuarioError($admin){
        $this->smarty->assign('admin', $admin);
        //le paso el admin para completar el navBar
        $this->smarty->assign('error', "No se pueden borrar usuarios que son administradores");
        $this->smarty->display('templates/error.tpl');
    }

    function renderCamposIncompletos(){
        $this->smarty->assign('error', "Faltan Completar Campos");
        $this->smarty->display('templates/error.tpl');
    }

    function renderUsuarioExistente(){
        $this->smarty->assign('error', "Ese usuario ya existe");
        $this->smarty->display('templates/error.tpl');
    }

    function showRegistro(){
        $this->smarty->display('templates/Usuarios/registro.tpl');
    }
}