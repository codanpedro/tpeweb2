<?php
require_once './libs/smarty-3.1.39/libs/Smarty.class.php';

class AnimalsView{
    private $smarty;

    function __construct() {
        //1. 
        $this->smarty = new Smarty();
    }

    function renderAnimals($animals, $logIn){
        $this->smarty->assign('listaAnimales', 'Lista de animales');
        $this->smarty->assign('animals', $animals); 
        $this->smarty->assign('logIn', $logIn); 
        $this->smarty->display('../templates/Animals/animales.tpl'); 
    }

    function renderAnimal($animal, $categoria, $admin, $id_usuario){
        $this->smarty->assign('animal', $animal);
        $this->smarty->assign('categoria', $categoria);
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('id_usuario', $id_usuario);
        $this->smarty->display('../templates/Animals/animal.tpl');
    }

    function renderFormularioAddAnimal($categorias){
        $this->smarty->assign('categorias', $categorias);
        $this->smarty->display('../templates/Animals/formularioAddAnimal.tpl');
    }
    function renderFormularioEditAnimal($animal, $categorias){
        $this->smarty->assign('animal', $animal);
        $this->smarty->assign('categorias', $categorias);
        $this->smarty->display('../templates/Animals/formularioEditAnimales.tpl');
    }

    function showInvitacion(){
        $this->smarty->assign('formulario','Podes hacer uso del formulario!');
        $this->smarty->display('../templates/Animals/animales.tpl'); 
    }

    function renderError(){
        $this->smarty->display('../templates/error.tpl');
    }

    function showHomeLocation(){
        header("Location: ".BASE_URL."showHome");
    }

    function renderCamposIncompletos($admin){
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('error','Faltan completar Campos, o tienen variables sin sentido');
        $this->smarty->display('../templates/error.tpl');
    }
}