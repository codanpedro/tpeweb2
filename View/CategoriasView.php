<?php
require_once './libs/smarty-3.1.39/libs/Smarty.class.php';
class CategoriasView{
    private $smarty;

    function __construct() {
        $this->smarty = new Smarty();
    }

    function renderCategorias($categorias, $logIn, $admin){              
        $this->smarty->assign('titulo', 'Lista de categorias');
        $this->smarty->assign('tiposAnimales', 'Tipo de animales');
        $this->smarty->assign('categorias', $categorias);
        $this->smarty->assign('logIn', $logIn);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('../templates/Categorias/categorias.tpl');   
        
    }

    function renderFormularioAddCategoria(){
        $this->smarty->display('../templates/Categorias/formularioAddCategoria.tpl');
    }

    function renderFormularioEditCategoria($categoria){
        $this->smarty->assign('categoria',$categoria);
        $this->smarty->display('../templates/Categorias/formularioEditCategorias.tpl');
    }

    function renderError(){
        $this->smarty->display('../templates/error.tpl');
    }

    function showHomeLocation(){
        header("Location: ".BASE_URL."showHome");
    }

    public function renderAnimalByCategoria($animals, $categoria, $admin){ 
        $this->smarty->assign('animals', $animals);
        $this->smarty->assign('categoria', $categoria);
        $this->smarty->assign('admin', $admin);
        $this->smarty->display('../templates/Animals/animalesCategoria.tpl'); 
    }

    function renderEliminarAnimals($admin){
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('error', "Hay que eliminar todos los animales previo a borrar una categoria");
        $this->smarty->display('../templates/error.tpl');
    }

    function renderCamposIncompletos($admin){
        $this->smarty->assign('admin', $admin);
        $this->smarty->assign('error', "Faltan completar campos");
        $this->smarty->display('../templates/error.tpl');
    }
}