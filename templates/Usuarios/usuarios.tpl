{include file='templates/header.tpl'}
<h3>Usuarios</h3>
    <table class="table">
        <thead>
            <tr>
                <th>Usuario</th>
            </tr>
        </thead>
        <tbody >
        {foreach from=$usuarios item=$usuario}
            <tr>
                <td> {$usuario->Usuario}</td>
                <td><a href="deleteUsuario/{$usuario->Usuario}" class="btn btn-danger"> Borrar</a></td>
                {if $usuario->Rol==1}
                    <td><a href="editRolUsuario/{$usuario->id_usuario}/0" class="btn btn-warning">Sacar Admin </a><td> 
                {else}
                    <td><a href="editRolUsuario/{$usuario->id_usuario}/1" class="btn btn-primary">Hacer Admin </a><td>
                {/if}
            </tr>
        {/foreach}
        </tbody>
    </table>
{include file='templates/footer.tpl'}
