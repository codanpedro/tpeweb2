{literal}
    <div id="app">
        <form class="form-inline" v-if="usuario" id="formularioComentario" v-on:submit.prevent="sendComentario" method="POST">
            <div>
                <label>Agregar Comentario</label>
                <textarea class="form-control" id="Comentario" name="Comentario" required placeholder="Comentario"></textarea>
                <label >Puntaje</label>
                <select name="Puntaje" class="custom-select">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>      
                <input type="submit" value="Comentar" class="btn btn-success">
            </div>
        </form>
        <h3>Comentarios</h3>
        <ul v-for="comentario in comentarios" >
            <li class="list-group-item">
                <h4>{{comentario.nombreUsuario}} dio un puntaje de {{comentario.Puntaje}}, y su comentario al respecto fue: </h4>
                <p>{{comentario.Comentario}}</p> 
                <div>
                    <button class="btn btn-danger" v-if="admin"  type="button" v-on:click="borrarComentario(comentario.id_comentario)">Eliminar</button>
                </div>
            </li>
        </ul>

    </div>
{/literal}