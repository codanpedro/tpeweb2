
<h1>{$listaAnimales}</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Raza</th>
            </tr>
        </thead>
        <tbody >
        {foreach from=$animals item=$animal}
            <tr>
                <td><a href="showAnimal/{$animal->id_animal}"> {$animal->Raza}</a> </td>
                {if $logIn}
                    <td><a href="deleteAnimal/{$animal->id_animal}" class="btn btn-danger"> Borrar</a></td>
                    <td><a href="showFormularioEditAnimals/{$animal->id_animal}" class="btn btn-warning">Editar</a></td>
                {/if}
            </tr>
        {/foreach}
        </tbody>
    </table>


