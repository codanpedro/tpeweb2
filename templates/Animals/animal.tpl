{include file='templates/header.tpl'}
    <h1>{$animal->Raza}</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Raza</th>
                <th>Tipo de Produccion</th>
                <th>Meses</th>
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Categoria</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{$animal->Raza} </td>
                <td>{$animal->Tipo_de_produccion}</td>
                <td>{$animal->Meses}</td>
                <td>{$animal->Cantidad}</td>
                <td>{$animal->Precio}</td>
                <td>{$categoria->Categorias}</td>
            </tr>
        </tbody>
    </table>
    
    <input type="hidden" id="id_animal" value={$animal->id_animal}>
    <input type="hidden" id="id_usuario" value={$id_usuario}>
    <input type="hidden" id="admin" value={$admin}>

    <div id="Comentarios">
            {include file='templates/vue/comentarios.tpl'}        
    </div>

    <script src="js/comentarios.js"></script>
{include file='templates/footer.tpl'}