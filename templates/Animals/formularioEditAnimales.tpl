<h3>Editar Animal</h3>
<form class="input-group mb-3" action="editAnimal" method="post">
    <input required class="form-control" type="hidden"  name="id_animal" id="id_animal" value="{$animal->id_animal}" >
    <input required class="form-control" type="text" name="Raza" id="Raza" placeholder="Raza" value="{$animal->Raza}" >
    <input required class="form-control" type="text" name="Tipo_de_produccion" id="Tipo_de_produccion" placeholder="Tipo de Produccion" value="{$animal->Tipo_de_produccion}">
    <input required class="form-control" type="number" name="Meses" id="Meses" placeholder="Meses" value="{$animal->Meses}">
    <input required class="form-control" type="number" name="Cantidad" id="Cantidad" placeholder="Cantidad" value="{$animal->Cantidad}" >
    <input required class="form-control" type="number" name="Precio" id="Precio" placeholder="precio" value="{$animal->Precio}">
    <select type="text" name="id_categoria" id="id_categoria">

        {foreach from=$categorias item=$categoria}
            {if $animal->id_categoria==$categoria->id_categoria}
                <option selected="true" value={$categoria->id_categoria}>{$categoria->Categorias}</option>
            
            {else} <option value={$categoria->id_categoria}>{$categoria->Categorias}</option>
            {/if}   
        {/foreach}

    </select>    
    <input type="submit" value="Editar Animal" class="btn btn-warning">
</form> 