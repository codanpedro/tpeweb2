{include file='templates/header.tpl'}
<h1>Animales {$categoria->Categorias}</h1>
<table class="table">
    <thead>
        <tr>
            <th>Raza</th>
            <th>Tipo de Produccion</th>
            <th>Meses</th>
            <th>Cantidad</th>
            <th>Precio</th>
        </tr>
    </thead>
    {foreach from=$animals item=$animal}
        <tbody>
                <tr>
                    <td> {$animal->Raza} </td>
                    <td> {$animal->Tipo_de_produccion}</td>
                    <td> {$animal->Meses}</td>
                    <td> {$animal->Cantidad}</td>
                    <td> {$animal->Precio}</td>
                
                </tr>
        </tbody>
    {/foreach}
</table>
{include file='templates/footer.tpl'}