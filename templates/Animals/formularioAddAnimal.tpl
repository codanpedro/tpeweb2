<h3>Agregar un Animal</h3>
<form class="input-group" action="addAnimal" method="post">
    <input required class="form-control" type="text" name="Raza" id="Raza" placeholder="Raza">
    <input required class="form-control" type="text" name="Tipo_de_produccion" id="Tipo_de_produccion" placeholder="Tipo de produccion">
    <input required class="form-control" type="number" name="Meses" id="Meses" placeholder="Meses">
    <input required class="form-control" type="number" name="Cantidad" id="Cantidad" placeholder="Cantidad">
    <input required class="form-control" type="number" name="Precio" id="Precio" placeholder="Precio">
    <select type="text" name="id_categoria" id="id_categoria">
        {foreach from=$categorias item=$categoria}
            <option value={$categoria->id_categoria}>{$categoria->Categorias}</option>
        {/foreach}
    </select>
    <input type="submit" value="Agregar Animal" class="btn btn-success">
</form> 
