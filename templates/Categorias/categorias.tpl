{if isset($smarty.session.usuario)}
    {include file='templates/header.tpl'}
{/if}

<h1>{$titulo}</h1>
    <table class="table">
        <thead>
            <tr>
                <th>{$tiposAnimales}</th>
            </tr>
        </thead>

        <tbody>
            {foreach from=$categorias item=$categoria}
                <tr>
                    <td><a href="showCategoria/{$categoria->id_categoria}"> {$categoria->Categorias}</a></td>
                    {if $logIn}
                        <td><a href="deleteCategoria/{$categoria->id_categoria}" class="btn btn-danger"> Borrar Categoria</a></td>
                        <td><a href="showFormularioEditCategoria/{$categoria->id_categoria}" class="btn btn-warning">Editar Categoria</a></td>
                    {/if}
                </tr> 
            {/foreach}
        </tbody>
    </table>