-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2021 at 02:44 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_agro`
--

-- --------------------------------------------------------

--
-- Table structure for table `animals`
--

CREATE TABLE `animals` (
  `id_animal` int(11) NOT NULL,
  `Raza` varchar(100) NOT NULL,
  `Tipo_de_produccion` varchar(100) NOT NULL,
  `Meses` int(100) NOT NULL,
  `Cantidad` int(100) NOT NULL,
  `Precio` int(255) NOT NULL,
  `id_categoria` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `animals`
--

INSERT INTO `animals` (`id_animal`, `Raza`, `Tipo_de_produccion`, `Meses`, `Cantidad`, `Precio`, `id_categoria`) VALUES
(10, 'Careta', 'Pastoreo', 12, 20, 3, 6),
(11, 'OvejasLocas', 'FeedLots', 1234, 20, 12, 7),
(13, 'Walter', 'Pastoreo', 12, 123, 333, 6),
(26, 'Gallos Peleadores', 'Clasico', 12, 20, 200, 15),
(27, 'Lechera', 'FeedLots', 12, 35, 3000, 6);

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `Categorias` varchar(40) NOT NULL,
  `Tamanio` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `Categorias`, `Tamanio`) VALUES
(6, 'Vacuno', 'Grande'),
(7, 'Ovino', 'Mediano'),
(15, 'Avicola', 'Pequenio-Mediano'),
(17, 'Exotica', 'Mediana');

-- --------------------------------------------------------

--
-- Table structure for table `comentarios`
--

CREATE TABLE `comentarios` (
  `id_comentario` int(11) NOT NULL,
  `Comentario` varchar(120) NOT NULL,
  `id_animal` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `Puntaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comentarios`
--

INSERT INTO `comentarios` (`id_comentario`, `Comentario`, `id_animal`, `id_usuario`, `Puntaje`) VALUES
(17, 'Este tipo de ovejas no termina de ser efectivo para un periodo anual', 11, 2, 3),
(23, 'Vamos a probar si esto sigue funcionando', 10, 2, 2),
(40, 'Muy buen rendimiento', 11, 2, 5),
(41, 'Buen rendimiento con pastoreo rustico', 10, 2, 4),
(44, 'Resisten con aguante la heladas', 10, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `Usuario` varchar(40) NOT NULL,
  `Contrasenia` varchar(100) NOT NULL,
  `Rol` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `Usuario`, `Contrasenia`, `Rol`) VALUES
(2, 'pedro', '$2y$10$pDSZBDeRYgB4y7ltb9s1v./4qJtwp9ijZbEARIDKQj1wQeC20hIqO', 1),
(8, 'pedro1', '$2y$10$Xnl8uVXS.ACtPz4FRFc9ouzzDViGSmqJAjgNCeJCF36rgUhcChpn2', 0),
(17, 'pedro123', '$2y$10$qZmm2VkDR5lcAMzPlKJpMufGqUtxPsTr1iu2pWmCCh3eHyq8taOAe', 1),
(19, 'pedro1234', '$2y$10$IO.vCMfZi/UzrMiMV.T4POhbj11ABdja01C0/KdvExfzmFJhta3/W', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animals`
--
ALTER TABLE `animals`
  ADD PRIMARY KEY (`id_animal`),
  ADD KEY `FK_id_productos` (`id_animal`) USING BTREE,
  ADD KEY `animals_ibfk_1` (`id_categoria`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id_comentario`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `comentarios_ibfk_1` (`id_animal`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animals`
--
ALTER TABLE `animals`
  MODIFY `id_animal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `animals`
--
ALTER TABLE `animals`
  ADD CONSTRAINT `animals_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`);

--
-- Constraints for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`id_animal`) REFERENCES `animals` (`id_animal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
