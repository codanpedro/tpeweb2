"use strict"
const API_URL = "api/comentarios";

let id_animal=document.querySelector("#id_animal").value;
id_animal=parseInt(id_animal);
let id_usuario = document.querySelector("#id_usuario").value;


let app = new Vue({
    el: "#app",
    data: {
        comentarios: [],
        admin: false,
        usuario: false
    },
    methods:{
        sendComentario: function(){agregarComentario()},
        borrarComentario: function(id_comentario){borrarComentario(id_comentario)}
    }
}); 

app.admin = document.querySelector("#admin").value;
app.usuario = id_usuario!=0;

async function getComentariosAnimal(id_animal) {
    try {
        let response = await fetch(API_URL + "/animal/" + id_animal);
        let comentarios = await response.json();
        if (response.ok){
            app.comentarios=comentarios;
        }else{   
            console.log("El animal no tiene comentarios");
            app.comentarios={};
        }
    } catch (e) {
        console.log(e);
    }
}

async function borrarComentario(id_comentario){
    try{
        let response= await  fetch(`${API_URL}/${id_comentario}`,{
            "method":"DELETE"
        });
        if (response.ok){
            console.log("Borrado correctamente");
            getComentariosAnimal(id_animal);
        }else{
            console.log("Error en el URL")
        }
    }
    catch (error){
        console.log(error);
    }
    
}

function generarJson(id_animal, comentario, id_usuario, puntaje) {
    let nuevoComentario = {
        "id_animal": id_animal,
        "id_usuario": id_usuario,
        "Comentario": comentario,
        "Puntaje": puntaje,
    }
    return nuevoComentario;
}


function agregarComentario(){
    let formComentario = document.querySelector("#formularioComentario");
    let formData = new FormData(formComentario);
    let puntaje = formData.get("Puntaje");
    puntaje = verificarPuntaje(puntaje);
    let comentario = formData.get("Comentario");
    let nuevoComentario = generarJson(id_animal, comentario, id_usuario, puntaje);
    postComentario(nuevoComentario);
}

function verificarPuntaje(puntaje){
    puntaje = parseInt(puntaje);
    if(puntaje>5){
        puntaje=5;
    };
    if (puntaje<1){
        puntaje=1;
    }
    return puntaje;
}

async function postComentario(comentarioNuevo){
    try{
        let response= await  fetch(API_URL,{
            "method":"POST",
            "headers": {'Content-type':"application/json"},
            "body": JSON.stringify(comentarioNuevo)
        });
        if (response.ok){
            console.log("Agregado correctamente");
            getComentariosAnimal(id_animal);
        }else{
            console.log("No se pudo traer el json de esa url")
        }
    }
    catch (error){
        console.log(error);
    }
}

getComentariosAnimal(id_animal);


