<?php 

require_once "Model/UsuariosModel.php";
require_once "View/LoginView.php";
require_once "Helpers/AuthHelper.php";

class LoginController {

    private $model;
    private $view;
    private $helper;

    function __construct(){
        $this->model = new UsuariosModel;
        //no hago un LoginModel porque tiene las mismas funciones que el UsuarioModel
        $this->view = new LoginView;
        $this->helper= new Helper;
    }

    function logout(){
        session_start();
        session_destroy();
        $this->view->showHomeLocation();
    }

    function login(){
        if (!$this->helper->checkLoggedIn()){
            $this->view->showLogin();
        }
    }

    function verifyLogin(){
        if (!empty($_POST['Usuario']) && !empty($_POST['Contrasenia'])) {
            $usuario = $_POST['Usuario'];
            $contrasenia = $_POST['Contrasenia'];
            $usuario = $this->model->getUsuario($usuario);    
            if ($usuario && password_verify($contrasenia, $usuario->Contrasenia)) {
                session_start();
                $_SESSION["usuario"] = $usuario;
                $this->view->showHomeLocation();
            } else {
                $this->view->renderCamposIncompatibles();
            }
        }else{
            $this->view->renderCamposIncompletos();
        } 
    }
    
      
    
}