<?php
require_once "View/AnimalsView.php";
require_once "Model/AnimalsModel.php";
require_once "Model/CategoriasModel.php";
require_once "Helpers/AuthHelper.php";
require_once "ComentariosController.php";

class AnimalsController{
    private $model;
    private $view;
    private $helper;
    private $modelCategorias;

    public function __construct(){
        $this->model = new AnimalsModel;
        $this->view = new AnimalsView;
        $this->helper = new Helper;
        $this->modelCategorias = new CategoriasModel;
        $this->comentariosController = new ComentariosController;
    }

    function showAnimals(){
        $animals=$this->model->getAnimals();
        $logIn=$this->helper->checkLoggedIn();
        $this->view->renderAnimals($animals, $logIn);
    }

    function showAnimal($id_animal){
        $animal=$this->model->getAnimal($id_animal);
        $id_categoria=$animal->id_categoria;
        $categoriaAnimal=$this->modelCategorias->getCategoria($id_categoria);
        $admin=$this->helper->esAdmin();
        $id_usuario = $this->helper->idUsuarioAcual();
        $this->view->renderAnimal($animal, $categoriaAnimal, $admin, $id_usuario);
    }

    function addAnimal(){
        if ($this->helper->checkLoggedIn() &&
            !empty($_POST['Raza']) &&
            !empty($_POST['Tipo_de_produccion']) &&
            !empty($_POST['Meses']) &&
            (($_POST['Meses'])>0) &&
            !empty($_POST['Cantidad']) &&
            (($_POST['Cantidad'])>0) &&
            !empty($_POST['Precio']) &&
            !empty($_POST['id_categoria'])){

            $this->model->insertAnimal($_POST['Raza'], $_POST['Tipo_de_produccion'], $_POST['Meses'],
            $_POST['Cantidad'], $_POST['Precio'], $_POST['id_categoria']);
            $this->view->showHomeLocation();    
        }else{
            //mando el admin para ejecutar el navBar
            $admin=$this->helper->esAdmin();
            $this->view->renderCamposIncompletos($admin);
        }
    }

    function deleteAnimal($id){
        if ($this->helper->checkLoggedIn()){
            $this->model->deleteAnimal($id);
            $this->view->showHomeLocation();
        } 
    }

    function editAnimal(){
        if ($this->helper->checkLoggedIn() &&
        !empty ($_POST['Raza']) &&
        !empty ($_POST['Tipo_de_produccion']) &&
        !empty ($_POST['Meses']) &&
        (($_POST['Meses'])>0) &&
        !empty($_POST['Cantidad']) &&
        (($_POST['Cantidad'])>0) && 
        !empty ($_POST['Precio']) &&
        !empty ($_POST['id_categoria'])) {

            $this->model->editAnimal($_POST['id_animal'], $_POST['Raza'], $_POST['Tipo_de_produccion'], $_POST['Meses'],
            $_POST['Cantidad'], $_POST['Precio'], $_POST['id_categoria']);
            $this->view->showHomeLocation();
        }else{
            //mando el admin para ejecutar el navBar
            $admin=$this->helper->esAdmin();
            $this->view->renderCamposIncompletos($admin);
        }

        
    }

    function showFormularioEditAnimal($id){
        if ($this->helper->checkLoggedIn()){
            $animal=$this->model->getAnimal($id);
            $categoria=$this->modelCategorias->getCategorias();
            $this->view->renderFormularioEditAnimal($animal, $categoria);
        }
    }
    
}