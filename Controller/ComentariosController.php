<?php
require_once "Model/UsuariosModel.php";
require_once "Model/AnimalsModel.php";
require_once "View/APIView.php";
require_once "Model/ComentariosModel.php";
require_once "Helpers/AuthHelper.php";

class ComentariosController{
    private $model;
    private $modelAnimal;
    private $helper;
    private $APIView;

    public function __construct(){
        $this->model = new ComentariosModel;
        $this->modelAnimal = new AnimalsModel;
        $this->modelUsuario = new UsuariosModel;
        $this->helper = new Helper;
        $this->APIView = new APIView;
    }

    function getComentariosAnimal($params = null){
        $id_animal = $params[":ID"];
        $comentarios = $this->model->getComentariosAnimal($id_animal);
        if ($comentarios) {
            return $this->APIView->response($comentarios, 200);
        }
        return $this->APIView->response("Parametros invalidos", 404);
    }

    function insertComentario(){
        $body = $this->getBody();
        if (empty ($_POST['Comentario']) && empty ($_POST['Puntaje'])){
            if($this->helper->checkLoggedIn()){
                $id_animal = $this->modelAnimal->getAnimal($body->id_animal);
                if ($id_animal==null){
                    return $this->APIView->response("Ese animal no existe", 404);  
                }
                if($body->Puntaje<6 && $body->Puntaje>0){
                    $id = $this->model->insertComentario($body->Comentario, $body->id_animal,
                    $body->id_usuario, $body->Puntaje);
                }
                if ($id != 0) {
                    return $this->APIView->response("El comentario se agregó con el id=$id", 200);
                } else {
                    return $this->APIView->response("El comentario no se pudo agregar", 500);
                }
            }else{
                return $this->APIView->response("Falta autorizacion", 401);
            }
        }
        else{
            return $this->APIView->response("Faltan completar campos", 404);
        }

    }

    function deleteComentario($params = null){
        if ($this->helper->esAdmin()){
            $id = $params[":ID"];
            $comentario = $this->model->getComentario($id);
            if ($comentario){
                $this->model->deleteComentario($id);
                return $this->APIView->response("Se elimino correctamente el comentario con id=$comentario", 200);
            }
            return $this->APIView->response("No hay comentario con id=$comentario", 404);
        }else{
            return $this->APIView->response("Falta autorizacion", 401);
        }
    }

    private function getBody() {
        $bodyString = file_get_contents("php://input");
        return json_decode($bodyString);
    }
}