<?php
require_once "View/CategoriasView.php";
require_once "Model/CategoriasModel.php";
require_once "Model/AnimalsModel.php";
require_once "View/AnimalsView.php";
require_once "Helpers/AuthHelper.php";

class CategoriasController{
    private $model;
    private $view;
    private $helper;
    private $modelAnimals;
    private $viewAnimals;

    public function __construct(){
        $this->model = new CategoriasModel;
        $this->view = new CategoriasView;
        $this->helper = new Helper;
        $this->modelAnimals=new AnimalsModel;
        $this->viewAnimals= new AnimalsView;
    }
    function showCategorias(){
        $categorias=$this->model->getCategorias();
        if ($categorias!=null){
            $logIn=$this->helper->checkLoggedIn();
            $admin=$this->helper->esAdmin();
            $this->view->renderCategorias($categorias, $logIn, $admin);   
        } else {
            $this->view->renderError();
        }
    }

    function showAnimalByCategery($id_categoria){
        $animals=$this->modelAnimals->getAnimalsByCategory($id_categoria);
        $categoriaAnimal=$this->model->getCategoria($id_categoria);
        $admin=$this->helper->esAdmin();
        //mando el admin para ejecutar el navBar
        $this->view->renderAnimalByCategoria($animals, $categoriaAnimal, $admin); 
    }

    function showFormularioEditCategoria($id){
        if ($this->helper->checkLoggedIn()){
            $categoria=$this->model->getCategoria($id);
            $this->view->renderFormularioEditCategoria($categoria);
        }
    }
    function deleteCategoria($id){
        if ($this->helper->checkLoggedIn()){
            $animalsEnCategoria = $this->modelAnimals->getAnimalsByCategory($id);
            if ($animalsEnCategoria==[]){
                $this->model->deleteCategoria($id);
                $this->view->showHomeLocation();
            }
            else {
                $admin=$this->helper->esAdmin();
                //mando el admin para ejecutar el navBar
                $this->view->renderEliminarAnimals($admin);
            }
        } 

    }
    function addCategoria(){
        if ($this->helper->checkLoggedIn() && 
            !empty ($_POST['Categoria']) &&
            !empty ($_POST['Tamanio'])){

            $this->model->insertCategoria($_POST['Categoria'], $_POST['Tamanio']);
            $this->view->showHomeLocation();
        }else{
            $admin=$this->helper->esAdmin();
            //mando el admin para ejecutar el navBar
            $this->view->renderCamposIncompletos($admin);
        }
        
    }
    function editCategoria(){
        if ($this->helper->checkLoggedIn() && 
            !empty($_POST['Categorias']) &&
            !empty($_POST['Tamanio'])){
                
            $this->model->editCategoria($_POST['id_categoria'], $_POST['Categorias'],
             $_POST['Tamanio']);
             $this->view->showHomeLocation();
        }else{
            $admin=$this->helper->esAdmin();
            //mando el admin para ejecutar el navBar
            $this->view->renderCamposIncompletos($admin);
        }
    }

    function showFormulario(){
        if ($this->helper->checkLoggedIn()){
            $categorias=$this->model->getCategorias();
            $this->viewAnimals->renderFormularioAddAnimal($categorias);
            $this->view->renderFormularioAddCategoria();
        }
    }
}