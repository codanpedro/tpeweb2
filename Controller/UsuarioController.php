<?php 

require_once "Model/UsuariosModel.php";
require_once "View/UsuariosView.php";
require_once "Helpers/AuthHelper.php";
require_once "Controller/LoginController.php";

class UsuarioController {

    private $model;
    private $view;
    private $helper;
    private $loginController;

    function __construct(){
        $this->model = new UsuariosModel;
        $this->view = new UsuariosView;
        $this->helper= new Helper;
        $this->loginController= new LoginController;
    }

    function deleteUsuario($usuario){
        $admin = $this->helper->esAdmin();
        if($admin){
            $usuario = $this->model->getUsuario($usuario);
            if($usuario != null && $usuario->Rol!=1){
                $this->model->deleteUsuario($usuario->id_usuario);
                $this->view->showUsuariosLocation();
            }else{
                $this->view->deleteUsuarioError($admin);
            }
            
        }
    }

    function showUsuarios(){
        if($this->helper->esAdmin()){
            $usuarios = $this->model->getUsuarios();
            $admin=$this->helper->esAdmin();
            $this->view->renderUsuarios($usuarios, $admin);
        }
        else {
            $this->view->showHomeLocation();
        }
    }

    function registroUsuario(){
        if(!empty($_POST['Usuario']) && !empty($_POST['Contrasenia'])){
            $usuario=$_POST['Usuario'];
            $existe=$this->model->getUsuario($usuario);
            if (!$existe){
                $userContrasenia=password_hash($_POST['Contrasenia'], PASSWORD_BCRYPT);
                $this->model->insertUsuario($usuario,$userContrasenia);
                $this->loginController->verifyLogin();
                $this->view->showHomeLocation();
            }else{
                $this->view->renderUsuarioExistente();
            }       
        }else{
            $this->view->renderCamposIncompletos();
        } 
    }

    function editRolUsuario($id, $rol){
        if ($this->helper->esAdmin()){
            $Admins = $this->model->getUsuariosAdmin();
            if ($rol==1 || sizeof($Admins)!=1){
                $this->model->editRolUsuario($id,$rol);
                $this->view->showUsuariosLocation();
                if ($this->helper->autoEdicion($id)){
                    //si me estoy sacando el admin a mi mismo me deslogueo
                    $this->loginController->logout();
                }
            }
            $this->view->showUsuariosLocation();
        }
    }

    function registro(){
        if (!$this->helper->checkLoggedIn()){
            $this->view->showRegistro();
        }
    } 
}