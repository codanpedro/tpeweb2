<?php

class AnimalsModel{
    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;'.'dbname=db_agro;charset=utf8', 'root', '');
    }

    function deleteAnimal($id){
        $sentencia = $this->db->prepare("DELETE FROM animals WHERE id_animal=?");
        $sentencia->execute(array($id));
    }
    function editAnimal($id, $raza,$tipoProduccion,$meses,$cantidad,$precio, $id_categoria){
        $sentencia = $this->db->prepare("UPDATE animals SET Raza=?, Tipo_de_produccion=?,
        Meses=?, Cantidad=?, Precio=?, id_categoria=? WHERE id_animal=?");
        $sentencia->execute(array($raza,$tipoProduccion,$meses,$cantidad,$precio, $id_categoria,$id));
    }
    function insertAnimal($raza, $tipoProduccion, $meses, $cantidad, $precio, $id_categoria){    
        $sentencia = $this->db->prepare("INSERT INTO animals(Raza, Tipo_de_produccion, Meses, 
        Cantidad, Precio, id_categoria) VALUES (?,?,?,?,?,?)");
        $sentencia->execute(array($raza, $tipoProduccion, $meses, $cantidad, $precio, $id_categoria));
    }
    public function getAnimalsByCategory($categoria){
        $sentencia = $this->db->prepare("SELECT * FROM animals WHERE id_categoria=?");
        $sentencia->execute(array($categoria)); 
        $categoria = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $categoria;
    }

    public function getAnimals(){
        $sentencia = $this->db->prepare("SELECT * FROM animals");
        $sentencia->execute([]); 
        $animals = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $animals;
    }
    
    public function getAnimal($id_animal){
        $sentencia = $this->db->prepare("SELECT * FROM animals WHERE id_animal=?");
        $sentencia->execute([$id_animal]);
        $animal = $sentencia->fetch(PDO::FETCH_OBJ);
        return $animal;
    }
}