<?php
class CategoriasModel{
    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;'.'dbname=db_agro;charset=utf8', 'root', '');
    }
    
    function deleteCategoria($id){

        $sentencia = $this->db->prepare("DELETE FROM categorias WHERE id_categoria=?");
        $sentencia->execute(array($id));
    }
    function editCategoria($id, $categoria, $tamanio){
        $sentencia = $this->db->prepare("UPDATE categorias SET Categorias=?, Tamanio=? WHERE id_categoria=?");
        $sentencia->execute (array($categoria, $tamanio, $id));
    }
    function insertCategoria($categoria, $tamanio){    
        $sentencia = $this->db->prepare("INSERT INTO categorias(Categorias, Tamanio) 
        VALUES (?,?)");
        $sentencia->execute(array($categoria,$tamanio));
    }

    public function getCategorias(){
        $sentencia = $this->db->prepare('SELECT * FROM categorias');
        $sentencia->execute([]); 
        $categorias = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $categorias;
    }

    public function getCategoria($id_categoria){
        $sentencia = $this->db->prepare("SELECT * FROM categorias WHERE id_categoria=?");
        $sentencia->execute(array($id_categoria)); 
        $categoria = $sentencia->fetch(PDO::FETCH_OBJ);
        return $categoria;
    }
}