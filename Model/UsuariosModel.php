<?php

class UsuariosModel{

    private $db;
    function __construct(){
         $this->db = new PDO('mysql:host=localhost;'.'dbname=db_agro;charset=utf8', 'root', '');
    }

    function getUsuario($us){
        $sentencia = $this->db->prepare('SELECT * FROM usuarios WHERE Usuario=?');
        $sentencia->execute(array($us));
        $usuario = $sentencia->fetch(PDO::FETCH_OBJ);
        return $usuario;
    }
    
    function insertUsuario($usuario, $contrasenia){
        $sentencia = $this->db->prepare('INSERT INTO usuarios(Usuario, Contrasenia) VALUES(?,?)');
        $sentencia->execute([$usuario, $contrasenia]);
    }

    function deleteUsuario($id){
        $sentencia = $this->db->prepare("DELETE FROM usuarios WHERE id_usuario=?");
        $sentencia->execute(array($id));
    }

    function getUsuarios(){
        $sentencia = $this->db->prepare('SELECT * FROM usuarios');
        $sentencia->execute([]);
        return $sentencia->fetchAll(PDO::FETCH_OBJ);
    }

    function editRolUsuario($id, $rol){
        $sentencia = $this->db->prepare("UPDATE usuarios SET Rol=? WHERE id_usuario=?");
        $sentencia->execute(array($rol, $id));
    }

    function getUsuariosAdmin(){
        $sentencia = $this->db->prepare('SELECT * FROM usuarios WHERE rol=1');
        $sentencia->execute([]);
        return $sentencia->fetchAll(PDO::FETCH_OBJ);
    }
}