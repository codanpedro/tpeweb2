<?php
class ComentariosModel{
    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;'.'dbname=db_agro;charset=utf8', 'root', '');
    }

    function getComentariosAnimal($id_animal){
        $sentencia = $this->db->prepare("SELECT comentarios.*, usuarios.usuario as nombreUsuario FROM comentarios JOIN usuarios ON comentarios.id_usuario = usuarios.id_usuario WHERE id_animal=?");
        $sentencia->execute(array($id_animal));
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios; 
    }

    function getComentarios(){
        $sentencia = $this->db->prepare("SELECT * FROM comentarios");
        $sentencia->execute(array());
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }

    function getComentario($id){
        $sentencia = $this->db->prepare( "SELECT * FROM comentarios WHERE id_comentario=?");
        $sentencia->execute(array($id));
        $comentario = $sentencia->fetch(PDO::FETCH_OBJ);
        return $comentario;
    }
    function insertComentario($comentario, $id_animal, $id_usuario, $puntaje){
        $sentencia = $this->db->prepare("INSERT INTO comentarios(Comentario, id_animal, id_usuario, Puntaje) VALUES(?, ?, ?, ?)");
        $sentencia->execute(array($comentario, $id_animal, $id_usuario, $puntaje));
        return $this->db->lastInsertId();
    }

    function deleteComentario($id){
        $sentencia = $this->db->prepare("DELETE FROM comentarios WHERE id_comentario=?");
        $response = $sentencia->execute(array($id));
        return $response;
    }


}