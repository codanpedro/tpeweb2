<?php

class Helper{

    function __construct(){
        //esto lo hago para que no se ejecute 2 veces el session_start()
        if (session_status()!=PHP_SESSION_ACTIVE)
        session_start();
    }
    function checkLoggedIn(){
        if(!isset($_SESSION["usuario"])){
            return false;
        } else return true;
    }

    function esAdmin(){
        if($this->checkLoggedIn()){
            return $_SESSION["usuario"]->Rol==1;
        }
    }

    function idUsuarioAcual(){
        if($this->checkLoggedIn()){
            return $_SESSION["usuario"]->id_usuario;
        }else{
            return 0;
        }
    }

    function autoEdicion($id){
        if($this->checkLoggedIn()){
            return ($_SESSION['usuario']->id_usuario == $id);
        }
    }
}