<?php

require_once 'libs/Router.php';
require_once 'Controller/ComentariosController.php';

$router = new Router();

$router->addRoute('comentarios/animal/:ID', 'GET', 'ComentariosController', 'getComentariosAnimal');
$router->addRoute('comentarios', 'POST', 'ComentariosController', 'insertComentario');
$router->addRoute('comentarios/:ID', 'DELETE', 'ComentariosController', 'deleteComentario');

$router->route($_GET["resource"], $_SERVER['REQUEST_METHOD']);