<?php

require_once "Controller/AnimalsController.php";
require_once "Controller/CategoriasController.php";
require_once "Controller/LoginController.php";
require_once "Controller/UsuarioController.php";
require_once "Helpers/AuthHelper.php";

define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

$animalsController= new AnimalsController();
$categoriasController= new CategoriasController();
$loginController = new LoginController();
$usuarioController = new UsuarioController();
$helper= new Helper();

if (!empty ($_GET['action'])){
    $action=$_GET['action'];
}else{
    $action='showHome';
}


$params=explode('/', $action);

switch ($params[0]){
    case 'login': 
        $loginController->verifyLogin(); 
        break;
    case 'logout': 
        $loginController->logout();
        break;
    case 'registro':
        $usuarioController->registroUsuario();
        $loginController->login();
        $categoriasController->showCategorias();
        $animalsController->showAnimals();
        $categoriasController->showFormulario();
        break;
    case 'addAnimal':
        $animalsController->addAnimal();
        break;
    case 'deleteAnimal':
        $animalsController->deleteAnimal($params[1]);
        break;
    case 'editAnimal':
        $animalsController->editAnimal();
        break;
    case 'showFormularioEditAnimals':
        $loginController->login();
        $categoriasController->showCategorias();
        $animalsController->showAnimals();
        $animalsController->showFormularioEditAnimal($params[1]);
        $categoriasController->showFormulario();
        break;
    case 'showFormularioEditCategoria':
        $loginController->login();
        $categoriasController->showCategorias();
        $categoriasController->showFormularioEditCategoria($params[1]);
        $animalsController->showAnimals();
        $categoriasController->showFormulario();
        break;   
    case 'addCategoria':
        $categoriasController->addCategoria();
        break;
    case 'deleteCategoria':
        $categoriasController->deleteCategoria($params[1]);
        break;
    case 'editCategoria':
        $categoriasController->editCategoria();
        break;
    case 'showAnimals':
        $animalsController->showAnimals();
        break;
    case 'showCategorias':
        $categoriasController->showCategorias();
        break;
    case 'showCategoria':
        $categoriasController->showAnimalByCategery($params[1]);
        break;
    case 'showAnimal':
        $animalsController->showAnimal($params[1]);
        break;
    case 'showUsuarios':
        $usuarioController->showUsuarios();
        break;
    case 'deleteUsuario':
        $usuarioController->deleteUsuario($params[1]);
        break;
    case 'editRolUsuario':
        $usuarioController->editRolUsuario($params[1], $params[2]);
    case 'showHome':
        $usuarioController->registro();
        $loginController->login();
        $categoriasController->showCategorias();
        $animalsController->showAnimals();
        $categoriasController->showFormulario();
        break;
}
